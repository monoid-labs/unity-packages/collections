using Unity.Collections;
using NUnit.Framework;

namespace Monoid.Unity.Collections {

  public static class NativeRingBufferTests {

    [Test]
    public static void TestAppend() {
      // not exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        rb.Append(0);
        AssertEquals(in rb, 0);
        rb.Append(1);
        AssertEquals(in rb, 0, 1);
        rb.Append(2);
        AssertEquals(in rb, 0, 1, 2);
      }
      // exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        // drop out
        rb.Append(1);
        AssertEquals(in rb, 1);
        rb.Append(2);
        AssertEquals(in rb, 1, 2);
        // last to fill in
        rb.Append(0);
        AssertEquals(in rb, 1, 2, 0);
        rb.Append(1);
        AssertEquals(in rb, 2, 0, 1);
        rb.Append(2);
        AssertEquals(in rb, 0, 1, 2);
      }
    }

    [Test]
    public static void TestPrepend() {
      // not exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        rb.Prepend(2);
        AssertEquals(in rb, 2);
        rb.Prepend(1);
        AssertEquals(in rb, 1, 2);
        rb.Prepend(0);
        AssertEquals(in rb, 0, 1, 2);
      }
      // exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        // drop out
        rb.Prepend(1);
        AssertEquals(in rb, 1);
        rb.Prepend(0);
        AssertEquals(in rb, 0, 1);
        // last to fill in
        rb.Prepend(2);
        AssertEquals(in rb, 2, 0, 1);
        rb.Prepend(1);
        AssertEquals(in rb, 1, 2, 0);
        rb.Prepend(0);
        AssertEquals(in rb, 0, 1, 2);
      }
    }


    [Test]
    public static void TestRemoveFirst() {
      // not exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        rb.Append(0);
        rb.Append(1);
        rb.Append(2);
        // remove first two
        rb.RemoveFirst();
        AssertEquals(in rb, 1, 2);
        rb.RemoveFirst();
        AssertEquals(in rb, 2);
        rb.Prepend(1);
        rb.RemoveFirst();
        AssertEquals(in rb, 2);
      }
      // exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        // drop out
        rb.Append(1);
        rb.Append(2);
        // last to fill in
        rb.Append(0);
        rb.Append(1);
        rb.Append(2);
        // remove first two
        rb.RemoveFirst();
        AssertEquals(in rb, 1, 2);
        rb.RemoveFirst();
        AssertEquals(in rb, 2);
        rb.Prepend(1);
        rb.RemoveFirst();
        AssertEquals(in rb, 2);
      }
    }

    [Test]
    public static void TestRemoveLast() {
      // not exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        rb.Append(0);
        rb.Append(1);
        rb.Append(2);
        // remove last two
        rb.RemoveLast();
        AssertEquals(in rb, 0, 1);
        rb.RemoveLast();
        AssertEquals(in rb, 0);
        rb.Append(1);
        rb.RemoveLast();
        AssertEquals(in rb, 0);
      }
      // exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        // drop out
        rb.Append(1);
        rb.Append(2);
        // last to fill in
        rb.Append(0);
        rb.Append(1);
        rb.Append(2);
        // remove last two
        rb.RemoveLast();
        AssertEquals(in rb, 0, 1);
        rb.RemoveLast();
        AssertEquals(in rb, 0);
        rb.Append(1);
        rb.RemoveLast();
        AssertEquals(in rb, 0);
      }
    }


    [Test]
    public static void TestRemoveAt() {
      // not exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        rb.Append(1);
        AssertEquals(in rb, 1);
        rb.Prepend(0);
        AssertEquals(in rb, 0, 1);
        rb.Append(2);
        AssertEquals(in rb, 0, 1, 2);
        // remove
        rb.RemoveAt(1);
        AssertEquals(in rb, 0, 2);
        rb.RemoveAt(0);
        AssertEquals(in rb, 2);
        rb.Prepend(0);
        rb.RemoveAt(1);
        AssertEquals(in rb, 0);
      }
      // exceeding the capacity
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        // drop out
        rb.Append(2);
        AssertEquals(in rb, 2);
        rb.Prepend(1);
        AssertEquals(in rb, 1, 2);
        // last to fill in
        rb.Append(0);
        AssertEquals(in rb, 1, 2, 0);
        rb.Prepend(0);
        AssertEquals(in rb, 0, 1, 2);
        // remove
        rb.RemoveAt(1);
        AssertEquals(in rb, 0, 2);
        rb.RemoveAt(0);
        AssertEquals(in rb, 2);
        rb.Prepend(0);
        rb.RemoveAt(1);
        AssertEquals(in rb, 0);
      }
    }

    [Test]
    public static void TestInsert() {
      using (var rb = new NativeRingBuffer<int>(3, Allocator.Temp)) {
        // -- not exceeding the capacity --
        // back
        rb.Insert(0, 2);
        AssertEquals(in rb, 2);
        // front
        rb.Insert(0, 0);
        AssertEquals(in rb, 0, 2);
        // middle
        rb.Insert(1, 1);
        AssertEquals(in rb, 0, 1, 2);
        // -- exceeding the capacity --
        // back
        rb.Insert(3, 4);
        AssertEquals(in rb, 1, 2, 4);
        // back-middle
        rb.Insert(2, 3);
        AssertEquals(in rb, 2, 3, 4);
        // front
        rb.Insert(0, 0);
        AssertEquals(in rb, 0, 2, 3);
        // front-middle
        rb.Insert(1, 1);
        AssertEquals(in rb, 0, 1, 2);
        rb.Insert(1, 1);
        AssertEquals(in rb, 0, 1, 1);
        // back-middle
        rb.Insert(2, 2);
        AssertEquals(in rb, 1, 2, 1);
      }
    }

    static void AssertEquals(in NativeRingBuffer<int> rb, params int[] values) {
      Assert.AreEqual(rb.Length, values.Length, "Length");
      for(int n=rb.Length, i=0; i<n; i++) {
        Assert.AreEqual(values[i], rb[i], $"[{i}]");
      }
    }

  }
}
