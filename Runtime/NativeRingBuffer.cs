using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
#if UNITY_2019_3_OR_NEWER
using Unity.Jobs;
#endif

namespace Monoid.Unity.Collections {

  /// `NativeRingBuffer<T>` is a fixed size array double ended queue.
  /// If the capacity limit is reached appending pushes out the first and prepending the last element.
  /// Inserting an element at an index will push out the element at the opposite end.
  /// Operations at the ends (`Prepend`, `Append`, `RemoveFirst`, `RemoveLast`) are `O(1)`.
  /// Operations in the middle (`Insert`, `RemoveAt`) are `O(n)`.
  [StructLayout(LayoutKind.Sequential)]
  [NativeContainer]
  [DebuggerDisplay("Length = {Length}")]
  public struct NativeRingBuffer<T> : IEnumerable<T>, IDisposable where T : struct {

    NativeArray<T> buffer;
    int count, offset;

    public NativeRingBuffer(int capacity, Allocator alloc) {
      buffer = new NativeArray<T>(capacity, alloc, NativeArrayOptions.UninitializedMemory);
      count = 0;
      offset = 0;
    }

    public int Length => count;

    public int Capacity => buffer.Length;

    public bool IsCreated => buffer.IsCreated;

    public T this[int index] {
      get {
        EnsureBounds(index);
        return this.buffer[(this.offset + index) % this.buffer.Length];
      }
      [WriteAccessRequired]
      set {
        EnsureBounds(index);
        this.buffer[(this.offset + index) % this.buffer.Length] = value;
      }
    }

    [WriteAccessRequired]
    public void Clear() {
      this.count = 0;
      this.offset = 0;
    }

    //-------------------------------------------------------------------------

    public void Resize(int capacity, Allocator alloc) {
      var newBuffer = new NativeArray<T>(capacity, alloc, NativeArrayOptions.UninitializedMemory);
      int n = Math.Min(capacity, this.Length);
      for (int i = 0; i < n; ++i) {
        newBuffer[i] = this[i];
      }
      this.buffer.Dispose();
      this.count = n;
      this.offset = 0;
      this.buffer = newBuffer;
    }

    //-------------------------------------------------------------------------

    [WriteAccessRequired]
    public void Append(in T sample) { // InsertAt(Length);
      this.buffer[(this.offset + this.count) % this.buffer.Length] = sample;
      if (this.count < this.buffer.Length) {
        this.count++;
      } else {
        this.offset = ++this.offset % this.buffer.Length;
      }
    }

    [WriteAccessRequired]
    public void Prepend(in T sample) { // InsertAt(0);
      if (this.count < this.buffer.Length) {
        this.count++;
      }
      this.offset = (--this.offset + this.buffer.Length) % this.buffer.Length;
      this.buffer[offset] = sample;
    }

    //-------------------------------------------------------------------------

    // deque, fast RemoveAt (0);
    [WriteAccessRequired]
    public T RemoveFirst() {
      this.EnsureLength(1);
      T first = this[0];
      --this.count;
      this.offset = (this.offset + 1) % this.buffer.Length;
      return first;
    }

    // pop, fast RemoveAt (Length - 1);
    [WriteAccessRequired]
    public T RemoveLast() {
      this.EnsureLength(1);
      T last = this[this.count - 1];
      --this.count;
      return last;
    }

    //-------------------------------------------------------------------------

    [WriteAccessRequired]
    public T RemoveAt(int index) {
      this.EnsureBounds(index);

      T elem = this[index];
      if (index <= this.count / 2) {
        for (int i = index; i > 0; --i) {
          this[i] = this[i - 1];
        }
        ++this.offset;
        --this.count;
      } else {
        for (int i = index + 1; i < this.count; ++i) {
          this[i - 1] = this[i];
        }
        --this.count;
      }
      return elem;
    }

    //-------------------------------------------------------------------------

    [WriteAccessRequired]
    public void Insert(int index, T elem) {
      this.EnsureBoundsPlusOne(index);

      if (this.count < this.buffer.Length) { // space for at least one more
        this.count++;
        if (2 * index < this.count) {
          this.offset = (--this.offset + this.buffer.Length) % this.buffer.Length;
          for (int i = 0; i < index; i++) {
            this[i] = this[i + 1];
          }
          this[index] = elem;
        } else {
          for (int i = this.count - 1; i > index; i--) {
            this [i] = this [i - 1];
          }
          this[index] = elem;
        }
      } else { // no more space -> one drop out at the opposite end
        if (2 * index < this.count) { // insert in first half -> push out last
          this.offset = (--this.offset + this.buffer.Length) % this.buffer.Length;
          for (int i = 0; i < index; i++) {
            this[i] = this[i + 1];
          }
          this[index] = elem;
        } else { // insert in seccond half -> push out first
          this.offset = ++this.offset % this.buffer.Length;
          for (int i = this.count - 1; i >= index; i--) {
            this[i] = this [i - 1];
          }
          this[--index] = elem;
        }
      }
    }

    //-------------------------------------------------------------------------

    #region Copy

    [WriteAccessRequired]
    public void CopyFrom(T[] array) => this.CopyFrom(array, array.Length);

    [WriteAccessRequired]
    public void CopyFrom(T[] array, int count, int offset = 0) {
      this.EnsureLength(count);
      for (int i = 0; i < count; ++i) {
        this[i] = array[i + offset];
      }
    }

    [WriteAccessRequired]
    public void CopyFrom(NativeArray<T> array) => this.CopyFrom(array.Slice());

    [WriteAccessRequired]
    public void CopyFrom(NativeSlice<T> slice) {
      for (int i = 0; i < this.count; ++i) {
        this[i] = slice[i];
      }
    }

    public void CopyTo(T[] array) => this.CopyTo(array, array.Length);

    public void CopyTo(T[] array, int count, int offset = 0) {
      EnsureLength(count);
      for (int i = 0; i < count; ++i) {
        array[i + offset] = this[i];
      }
    }

    public void CopyTo(NativeArray<T> array) => this.CopyTo(array.Slice());

    public void CopyTo(NativeSlice<T> slice) {
      EnsureLength(slice.Length);
      for (int i = 0; i < this.count; ++i) {
        slice[i] = this[i];
      }
    }

    public T[] ToArray() {
      var array = new T[Length];
      CopyTo(array);
      return array;
    }

    #endregion

    #region IDisposable

    [WriteAccessRequired]
    public void Dispose() => this.buffer.Dispose();

#if UNITY_2019_3_OR_NEWER
    public JobHandle Dispose(JobHandle inputDeps) => this.buffer.Dispose(inputDeps);
#endif

    #endregion

    #region IEnumerable<T>

    public IEnumerator<T> GetEnumerator() => new Enumerator(ref this);

    IEnumerator<T> IEnumerable<T>.GetEnumerator() => throw new NotImplementedException();

    IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();

    [UnityEngine.Internal.ExcludeFromDocs]
    public struct Enumerator : IEnumerator<T> {
      readonly NativeRingBuffer<T> buffer;
      int index;

      public Enumerator(ref NativeRingBuffer<T> buffer) {
        this.buffer = buffer;
        this.index = -1;
      }

      public T Current => this.buffer[index];
      object IEnumerator.Current => this.Current;

      public bool MoveNext() => ++this.index < this.buffer.Length;

      public void Reset() => this.index = -1;

      public void Dispose() { }
    }

    #endregion

    #region Checks

    [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
    void EnsureLength(int length) {
      if (length > this.count) {
        throw new ArgumentOutOfRangeException($"{nameof(NativeRingBuffer<T>)} has less than {length} elements ({this.count}).");
      }
    }

    [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
    void EnsureCapacity(int capacity) {
      if (capacity > this.buffer.Length) {
        throw new ArgumentOutOfRangeException($"{nameof(NativeRingBuffer<T>)} has less than {capacity} elements.");
      }
    }

    [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]

    void EnsureBounds(int index) {
      if (index < 0) {
        throw new ArgumentOutOfRangeException($"{nameof(NativeRingBuffer<T>)}: Index ({index}) cannot be less than zero.");
      }
      if (index >= this.count) {
        throw new ArgumentOutOfRangeException($"{nameof(NativeRingBuffer<T>)} Index ({index}) cannot be greater or equal than the length ({this.count}).");
      }
    }

    [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]

    void EnsureBoundsPlusOne(int index) {
      if (index < 0) {
        throw new ArgumentOutOfRangeException($"{nameof(NativeRingBuffer<T>)}: Index ({index}) cannot be less than zero.");
      }
      if (index > this.count) {
        throw new ArgumentOutOfRangeException($"{nameof(NativeRingBuffer<T>)} Index ({index}) cannot be greater than the length ({this.count}).");
      }
    }

    #endregion

  }

}
